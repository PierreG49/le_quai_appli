# Le Quai

Le but de cette appli est de fournir un outil d'analyse pour le théâtre Le Quai - CDN à Angers.
Cette appli repose sur la base de https://github.com/Ivaskuu/tinder_cards 
J'ai repris le même concept de cartes et de physique.
<img alt='Appli' src='res/screenshot.jpg' height='48px'/>

# Stage SIO

Cette appli a été developpé dans le cadre de mon stage de deuxime année en BTS SIO ( Service Informatique aux Organisations ) option SLAM ( Solutions Logicielles et Applications Métiers )

<hr>

Pour plus d'infos sur flutter, voici la [documentation](https://flutter.io/).